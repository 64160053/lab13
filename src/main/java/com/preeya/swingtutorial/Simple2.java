package com.preeya.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyFreme extends JFrame {

    JButton button;

    MyFreme() {

        super("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}
    public class Simple2 {
        public static void main(String[] args) {
            MyFreme freme = new MyFreme();
            freme.setVisible(true);
        }
    }
